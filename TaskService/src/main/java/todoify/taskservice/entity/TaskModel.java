package todoify.taskservice.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class TaskModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title")
    @NotEmpty
    private String title;

    @Column(name = "date_time")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date utilTimestamp;

    @Column(name = "description")
    @NotEmpty
    private String description;

    public TaskModel() {
    }

    public TaskModel(String title, Timestamp utilTimestamp, String description) {
        this.title = title;
        this.utilTimestamp = utilTimestamp;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getUtilTimestamp() {
        return utilTimestamp;
    }

    public void setUtilTimestamp(Timestamp utilTimestamp) {
        this.utilTimestamp = utilTimestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
