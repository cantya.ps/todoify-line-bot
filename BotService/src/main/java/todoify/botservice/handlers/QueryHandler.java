package todoify.botservice.handlers;

import com.linecorp.bot.model.message.Message;

import java.util.ArrayList;

public abstract class QueryHandler {
    ArrayList<String> acceptedQuery;
    String replyToken;

    QueryHandler(ArrayList<String> acceptedQuery) {
        this.acceptedQuery = acceptedQuery;
    }

    public abstract void handleQuery(String query, String argument,String replyToken,String userId);

    public void setAcceptedQuery(ArrayList<String> acceptedQuery) {
        this.acceptedQuery = acceptedQuery;
    }
}

